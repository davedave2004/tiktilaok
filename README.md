# Tiktilaok - Developed with CakePHP

[![Latest Stable Version](https://poser.pugx.org/cakephp/cakephp/v/stable.svg)](https://packagist.org/packages/cakephp/cakephp)
[![License](https://poser.pugx.org/cakephp/cakephp/license.svg)](https://packagist.org/packages/cakephp/cakephp)
[![Bake Status](https://secure.travis-ci.org/cakephp/cakephp.png?branch=master)](https://travis-ci.org/cakephp/cakephp)
[![Code consistency](https://squizlabs.github.io/PHP_CodeSniffer/analysis/cakephp/cakephp/grade.svg)](https://squizlabs.github.io/PHP_CodeSniffer/analysis/cakephp/cakephp/)

Tiktilaok is a microblog created by me as the result of the microblog exercise.

##Main Features:
###1. Account System
- An account is needed to do most of the things in Tiktilaok. Posting, following other, and etc.
- A valid username and email are all that you need. It will be unique per user.
###2. Follower System
- Only posts of people you follow will appear on your feed but you can still search for others.
- You can view yours and other people's followers and those you/they are following.
###3. Posting
- You can post messages, pictures, or both!
- You can also repost other people's post for all your followers to see!

