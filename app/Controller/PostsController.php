<?php
	class PostsController extends AppController {


		public function beforeFilter(){
			parent::beforeFilter();
			$this->Auth->allow(array('getpost')); //Add login
		}

		public function post(){

			$this->autoRender = false;

			if ($this->request->is("post")) {
				$loggedIn = $this->Auth->user('id');
				$this->request->data['Post']['user_id'] = $loggedIn;

				$this->Post->create();

				$params = array();

				//if quickpost or edit
				if (isset($this->request->data['Post']['postid'])) {
					$postid = $this->request->data['Post']['postid'];

					//if edit
				 	if (!empty($postid)) {
						$this->Post->id = $postid;

						$condition = array(
							'id' => $postid,
							'user_id' => $loggedIn
						);

						if (!$this->Post->hasAny($condition)) {
							die("409");
						}

						//if picture was not attached, edit only the content
						if ($this->request->data['Post']['attached']['error'] === 4) {
							$params['fieldList'] = array('content');
						}
					}
				}

				$this->Post->set($this->request->data);
				if ($this->Post->validates()) {
					if ($this->Post->save($this->request->data,$params)) {
						if ($this->request->is('ajax')) {
							return "Posted";
						}
						$this->Flash->success('Posted!');
					}
				} else {
				    // didn't validate logic
					if ($this->request->is('ajax')) {

						$allerrors = array();
					    foreach ($this->Post->validationErrors as $field => $errors) {
					    	foreach ($errors as $key => $error) {
					    		array_push($allerrors, $error);
					    	}
					    }
						return implode(", ", $allerrors);
					}

				    foreach ($this->Post->validationErrors as $field => $errors) {
				    	foreach ($errors as $key => $error) {
				    		$this->Flash->error($error);
				    	}
				    }
				}
			}
			return $this->redirect(array("controller" => "users", "action" => "index"));
		}

		public function delete($postid = null) {
			$this->autoRender = false;
			if (!$this->request->is('ajax')) {
				die();
			}
			if (is_null($postid)){
				die();
			}
			$loggedIn = $this->Auth->user('id');

			$condition = array(
				'id' => $postid,
				'user_id' => $loggedIn
			);

			if (!$this->Post->hasAny($condition)) {
				die("409");
			}

			if ($this->Post->delete($postid)) {
				return "deleted";
			} else {
				return "Could not delete";
			}


		}

		public function like() {
			if(!$this->request->is('post')) {
				return;
			}
			$postid = $this->request->data['likeThis'];

			if (!$this->Post->ifPostExists($postid)) {
				die("404");
			}

			$id = $this->Auth->user('id');
			$this->loadModel('Like');
			$likes = $this->Like->likePost($postid,$id);
			$this->set("result",$likes);
			$this->set('_serialize',array('result'));
		}


		public function getpost($postid) {
			if (!$this->request->is('ajax')) {
				return;
			}

			if (!$this->Post->ifPostExists($postid)) {
				die("404");
			}

			if ($this->Auth->loggedIn()) {
				$id = $this->Auth->user('id');
			} else {
				$id = null;
			}

			$post = $this->Post->getPost($postid,$id);
			$post = $this->Post->attachReposts($post);
			$this->loadModel("Comment");
			$post['Comments'] = $this->Comment->getComments($postid);
			$this->set("postinfo",$post);
		}

		public function repost() {
			if (!$this->request->is('ajax')) {
				return;
			}
			$reposted_post_id = $this->request->data['repostpostid'];
			$id = $this->Auth->user('id');
			$data = array(
				'user_id' => $id,
				'content' => $this->request->data['content'],
				'is_repost' => 1,
				'reposted_post_id' => $this->request->data['repostpostid']
			);

			$this->Post->create();
			if ($this->Post->save($data)) {
				$this->set("result",1);
			} else {
				$this->set("result",0);
			}
			$this->set("count",$this->Post->getRepostCount($reposted_post_id));
			$this->set('_serialize',array('result','count'));
		}

		public function getrepost($postid) {
			if (!$this->request->is('ajax')) {
				return;
			}
			if (!$this->Post->ifPostExists($postid)) {
				die("404");
			}

			$post = $this->Post->getRepost($postid);
			$this->set("postinfo",$post);
		}

		public function comment() {
			if (!$this->request->is('ajax')) {
				return;
			}

			$postid = $this->request->data['postid'];

			if (!$this->Post->ifPostExists($postid)) {
				die("404");
			}

			$id = $this->Auth->user('id');
			$data = array(
				'user_id' => $id,
				'comment' => $this->request->data['comment'],
				'post_id' => $postid,
			);

			$this->loadModel('Comment');
			$this->Comment->create();
			if ($this->Comment->save($data)) {
				$this->set("result",1);
			} else {
				$this->set("result",0);
			}
			$this->set("count",$this->Comment->getCommentCount($postid));
			$this->set('_serialize',array('result','count'));
		}

		public function getpostcontents() {
			if (!$this->request->is('ajax')) {
				die();
			}

			$postid = $this->request->query['postid'];
			if (!$this->Post->ifPostExists($postid)) {
				die("404");
			}

			$post = $this->Post->getPost($postid,null,false);

			$contents = array(
				'content' => $post['Post']['content'],
				'picture' => $post['Post']['picture']
			);

			$this->set("contents",$contents);
			$this->set("_serialize",'contents');
		}
	}
?>

