<?php
	class SearchController extends AppController {

		public $uses = array();

		function beforeFilter() {
			parent::beforeFilter();
			$this->Auth->allow("index");
		}

		function index(){
			$hideSearchBar = true;
			$user_class = array("search-button");
			$post_class = array("search-button");

			if (array_key_exists('kw', $this->params['url'])) {
				$keyword = $this->params['url']['kw'];
			}
			if (isset($keyword)) {
				if (array_key_exists('find', $this->params['url'])) {
					$category = $this->params['url']['find'];

					switch ($category) {
						case 'posts': {
							array_push($post_class, "active");
							break;
						}
						default: {
							array_push($user_class, "active");
							break;
						}
					}

				} else {
					$category = "users";
					array_push($user_class, "active");
				}

				$results = $this->_searchFor($category,$keyword);

				$hideSearchBar = false;
				$this->view = 'result';
				$this->set('category',$category);
				$this->set('user_class',$user_class);
				$this->set('post_class',$post_class);
				$this->set('keyword',$keyword);
				$this->set('results',$results);
			}
			$this->set('hideSearchBar',$hideSearchBar);

		}

		protected function _searchFor($category,$kw){
			$loggedId = null;
			if ($this->Auth->loggedIn()) {
				$loggedId = $this->Auth->user('id');
			}

			$word = "%$kw%";
			switch ($category) {
				case 'posts': {

					$this->loadModel('Post');
					$options = $this->Post->getAllPosts(null,true,$loggedId);
					$options['conditions'] = array('content LIKE ?' => $word);

					$this->Paginator->settings = $options;
				    try {
				        $results = $this->Paginator->paginate('Post');
				    } catch (NotFoundException $e) {
				    	$this->redirect(
				    		array(
				    			"controller" => $this->params['controller'],
				    			"action" => $this->params['action']
				    		)
				    	);
				    }

				    if (!empty($results)) {
				    	$results = $this->Post->attachReposts($results);

				    }

					break;
				}
				default: {

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

					//default = search for users
					if (isset($loggedId)) {
						//$contain = false;
						$contain = "Followers.follower_id = ".$loggedId;
					} else {
						$contain = false;
					}

					$options = array(
						'conditions' => array(
							'OR' => array(
								'username LIKE ?' => $word,
								'full_name LIKE ?' => $word
							)
						),
						'fields' => array(
							'id',
							'username',
							'full_name',
							'profile_pic',
							'description',
							'created'
						),
						'limit' => 10,
						'order' => 'User.full_name ASC',
						'contain' => $contain
					);

					$this->loadModel('User');
					$this->Paginator->settings = $options;

				    try {
				        $results = $this->Paginator->paginate('User');
				    } catch (NotFoundException $e) {
				    	$this->redirect(
				    		array(
				    			"controller" => $this->params['controller'],
				    			"action" => $this->params['action']
				    		)
				    	);
				    }
					break;
				}
			}
			return $results;
		}
	}
?>
