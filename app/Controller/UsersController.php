<?php
	App::uses('CakeEmail', 'Network/Email');

	class UsersController extends AppController {

		public $components = array('Emailer');

		public function beforeFilter(){
			parent::beforeFilter();
			$this->Auth->allow(array('register', 'profile', 'view', 'verify'));
		}

		public function login() {
			$this->layout = "noheader";

			if ($this->Auth->loggedIn()) {
				return $this->redirect($this->Auth->redirectUrl());
			}

			if ($this->request->is('post')) {
				if ($this->Auth->login()) {
					$userinfo = $this->Auth->user();
					if ($userinfo['is_verified']) {
						return $this->redirect($this->Auth->redirectUrl());
					} else {
						$this->Auth->logout();
						return $this->redirect(
							array('action' => 'verify',
								"?" => array(
									"user" => $userinfo['username'],
								)
							)
						);
					}
				}
				$this->Flash->error(__('Invalid username or password, try again'));
			}
		}

		public function logout() {
			return $this->redirect($this->Auth->logout());
		}

		public function index(){
			$id = $this->Auth->user('id');

			$info = $this->User->getUserInfo($id); //pag find by username magagamit to sa profile
			$followings = $info['Following'];

			$userinfo = $this->_prepareUserInfo($info);

			$followingIds = array($id);
			foreach ($followings as $key => $following) {
				array_push($followingIds, $following['user_id']);
			}

			$allposts = $this->_preparePostsToShow($followingIds);

			$this->getsuggestions($id,$followingIds);
			$this->set("allposts",$allposts);
			$this->set("data",$userinfo);
		}

		public function profile($username = null) {
			if (!$username || !$this->User->hasAny(array("User.username" => $username ))) {
				$this->redirect(array("controller" => "users", "action" => "login"));
			} else {
				$loggedId = null;
				if ($this->Auth->loggedIn()) {
					$loggedId = $this->Auth->user('id');
				}

				$info = $this->User->getUserInfo($username);
				$userinfo = $this->_prepareUserInfo($info);
				$allposts = $this->_preparePostsToShow($userinfo['id']);

				$this->set('allposts',$allposts);
				$this->set('user',$userinfo);
			}
		}

		public function register() {

			if ($this->request->is('post')) {
				$this->User->create();
				$this->User->set($this->request->data);

				if ($this->User->validates()) {
					if ($this->User->save($this->request->data,array('validate' => false))) {
						$email = $this->User->field('email');
						$username =  $this->User->field('username');
						$token = $this->User->field('verification_token');

						$sent = $this->Emailer->sendVerification($email,$username,$token);

						if ($sent['error']) {
							$this->Flash->error($sent['message']);
						} else {
							$this->Flash->success("Verification sent!");
						}

						return $this->redirect(
							array('action' => 'verify',
								"?" => array(
									"user" => $username,
								)
							)
						);
					}
				}
			}
			$this->set('hideSearchBar',true);
		}

		public function edit($category = null) {
			$id = $this->Auth->user('id');

			$user = $this->User->find(
				'first',
				array(
					'conditions' => array(
						'id' => $id
					),
					'contain' => false
				)
			);

			switch ($category) {
				case 'password':
					//set different view
					$params = array('fieldList' => array('oldpassword','password'));
					break;

				default:
					$category = 'general';
					$params = array('fieldList' => array('full_name','description'));
					break;
			}

			$this->set('category',$category);

			if ($this->request->is(array('post', 'put'))) {

				$this->User->id = $id;
				$this->requesst->data['User']['id'] = $id;

				if (isset($this->request->data['User']['picture'])) {

					$file = $this->request->data['User']['picture'];

					$this->User->set($this->request->data);
					if($this->User->validates()) {
						if ($this->User->updateProfilePicture($id,$file)) {
							$this->Flash->success(__('Your picture has been updated.'));
							return $this->redirect(array('action' => 'edit',$category));
						}
					}

				} else {
					if ($this->User->save($this->request->data,$params)) {
						$this->Flash->success(__('Your profile has been updated.'));
						return $this->redirect(array('action' => 'edit',$category));
					}
				}

				$this->Flash->error(__('Unable to update your profile.'));
				$this->request->data = $user;

			}
			if (!$this->request->data) {
				$this->request->data = $user;
			}
		}

		public function view($category,$identifier=null) {
			//check validity of identifier
			if (is_null($identifier) && ($category === 'followers' || $category === 'following')) {
				//if category is for following/followers
				//if no identifier was provided, assume loggedInUser
				if ($this->Auth->loggedIn()) {
					$identifier = $this->Auth->user('username');
				} else {
					$this->redirect(array("controller" => "users", "action" => "login"));
				}

			} elseif ((is_null($identifier) || !is_numeric($identifier)) && ($category === 'liked' || $category === 'reposted')) {
				//if category is for posts
				//if no identifier was provided or , nothing can be done. redirect
				$this->redirect(array("controller" => "users", "action" => "login"));
			}

			if ($category === 'followers' || $category === 'following') {
				if ($this->User->hasAny(array('username' => $identifier))) {
					$username = $identifier;
					$identifier = $this->User->getIdByUsername($username);
					$this->loadModel('Follower');

				} else {
					//No such user, redirect to login
					$this->redirect(array("controller" => "users", "action" => "login"));
				}

			} elseif ($category === 'liked' || $category === 'reposted') {
				$this->loadModel('Post');
				if (!$this->Post->hasAny(array('id' => $identifier))) {
					//No such post, redirect to login
					$this->redirect(array("controller" => "users", "action" => "login"));
				}
			}

			switch ($category) {
				case 'followers': {
					$this->set("view","category is $category");
					$ids = $this->Follower->getFollowers($identifier);
					$message = "Followers of ".$username;
					break;
				}
				case 'following': {
					$this->set("view","category is $category");
					$ids = $this->Follower->getFollowing($identifier);
					$message = "Who ".$username." is following";
					break;
				}
				case 'liked': {
					$this->set("view","category is $category");
					$message = "Who liked this post";
					$this->loadModel('Like');
					$ids= $this->Like->getWhoLikedThisPost($identifier);
					break;
				}
				case 'reposted': {
					$this->set("view","category is $category");
					$message = "Who reposted this post";

					$ids= $this->Post->getWhoRepostedThisPost($identifier);
					break;
				}
				default: {
					$this->redirect(array("controller" => "users", "action" => "login"));
					break;
				}
			}
			if ($this->Auth->loggedIn()) {
				$loggedInUser = $this->Auth->user('id');
			} else {
				$loggedInUser = null;
			}
			$options= $this->User->getInfoByIds($ids,true,$loggedInUser);
			$this->Paginator->settings = $options;
			try {
				$infos = $this->Paginator->paginate('User');
			} catch (NotFoundException $e) {
				$this->redirect(
					array(
						"controller" => $this->params['controller'],
						"action" => $this->params['action']
					)
				);
			}

			$this->set("infos",$infos);
			$this->set("message",$message);
		}

		public function getsuggestions($id=null,$followingIds=null){
			if ($this->request->is('ajax')) {
				$this->view = "ajax/suggestions";
				$id = $this->Auth->user('id');
			}
			$this->loadModel('Follower');

			if (is_null($followingIds)) {
				$followingIds = $this->Follower->getFollowing($id);
			}

			$suggestions = $this->User->getSuggestionsFor($id,$followingIds);
			$this->set('suggestions',$suggestions);
		}

		public function refreshfeed(){

			$this->view = 'ajax/refreshfeed';

			$id = $this->request->data['id'];
			$page = $this->request->data['page'];

			if (isset($this->request->data['action'])) {
				$action = $this->request->data['action'];
			} else {
				$action = 'index';
			}

			$filteredParams = array();
			if (isset($this->request->data['params'])) {
				$rawParams = explode("/",$this->request->data['params']);
				foreach ($rawParams as $key => $param) {
					//if param is page, ignore
					if (preg_match("/page:/i",$param)) {
						continue;
					}
					array_push($filteredParams, $param);
				}
			}

			if ($this->Auth->loggedIn()) {
				$loggedInUser = $this->Auth->user('id');
			}

			if (empty($id)) {
				$id =  $loggedInUser;
			}

			$info = $this->User->getUserInfo($id); //pag find by username magagamit to sa profile

			if (is_numeric($id)) {
				$info = $this->User->getUserInfo($id); //pag find by username magagamit to sa profile
				$followings = $info['Following'];
				$followingIds = array($id);
				foreach ($followings as $key => $following) {
					array_push($followingIds, $following['user_id']);
				}

			} elseif (is_string($id)) {
				$followingIds = array($info['User']['id']);
			}

			if (empty($page) || !is_numeric($page)) {
				$page =  1;
			}

			$allposts = $this->_preparePostsToShow($followingIds,$page,$action);
			$this->set('currentParams',$filteredParams);
			$this->set('currentAction',$action);
			$this->set('allposts',$allposts);
		}

		//ajax functions
		public function follow() {

			if (!$this->request->is('ajax')) {
				return;
			}
			$user = $this->Auth->user('id');
			$toFollow = $this->request->data['followThis'];

			$this->loadModel('Follower');
			$result = $this->Follower->follow($toFollow,$user);

			$this->set("result",$result['result']);
			$this->set("follower_count",$result['fer']);
			$this->set("following_count",$result['fing']);
			$this->set('_serialize',array('result','follower_count','following_count'));
		}


		public function getprofilestats() {

			if (!$this->request->is('ajax')) {
				throw new Exception("Error Processing Request", 1);
			}

			$id = $this->request->data['id'];

			if (!is_string($id)) {
				$this->set("error","Not a valid id");
				$this->set('_serialize',array('error'));
				return;
			}

			$info = $this->User->getUserInfo($id);

			if (empty($info)) {
				$this->set("error","No such user");
				$this->set('_serialize',array('error'));
				return;
			}

			$userinfo = $this->_prepareUserInfo($info);

			$this->set("follower_count",$userinfo['follower_count']);
			$this->set("following_count",$userinfo['following_count']);
			$this->set("like_count",$userinfo['like_count']);
			$this->set("post_count",$userinfo['post_count']);
			$this->set("repost_count",$userinfo['repost_count']);
			$this->set('_serialize',array('follower_count','following_count','like_count','repost_count','post_count'));
		}

		public function verify(){
			$token = $this->request->query('token');
			$user = $this->request->query('user');

			$existing = $this->User->hasAny(
				array(
					"username" => $user
				)
			);

			if ((is_null($user) && is_null($token)) || is_null($user) || !$existing) {
				$message = "Something seems to be wrong with the link. Please check your email again";
				$buttonmessage = array(
					"message" => "Go back to home",
					"link" => array(
						'controller' => 'users',
						'action' => 'login'
					)
				);
			} else {
				//check if verifeid na ung user
				$verified = $this->User->hasAny(
					array(
						"username" => $user,
						"is_verified" => true
					)
				);
				if ($verified) {
					$this->Flash->success("Your account is now verified");
					return $this->redirect(array("controller" => "users", "action" => "login"));
				}

				if (is_null($token)) {
					$message = "Your account still needs to be verified before you can start. Check your email";

					$buttonmessage = array(
						"message" => "Resend Email",
						"link" => array(
							'controller' => 'users',
							'action' => 'verify',
							'?' => array(
								'resend' => 1,
								'user' => $user
							)
						)
					);

					$resend = $this->request->query('resend');
					if(!is_null($resend) || $resend === 1) {

						$info = $this->User->getVerificationTokenByUsername($user);

						$sent = $this->Emailer->sendVerification($info['User']['email'],$info['User']['username'],$info['User']['verification_token']);

						if ($sent['error']) {
							$this->Flash->error($sent['message']);
						} else {
							$this->Flash->success("Verification sent!");
						}

						return $this->redirect(
							array('action' => 'verify',
								"?" => array(
									"user" => $user,
								)
							)
						);
					}


				} else {
					$result = $this->User->verifyToken($user,$token);
					if ($result['error']) {
						$message = $result['message'];
					} else {
						$message = "Congratulations, ".$result['user']['User']['full_name'].", You can now start tikilaoking";
						$buttonmessage = array(
							"message" => "Start Tiktilaok-ing",
							"link" => array(
								'controller' => 'users',
								'action' => 'login',
							)
						);
					}
				}
			}
			$this->set('buttonmessage',$buttonmessage);
			$this->set('message',$message);
		}

		protected function _prepareUserInfo($info) {

			if ($this->Auth->loggedIn()) {
				$loggedId = $this->Auth->user('id');
			} else {
				$loggedId = null;
			}

			$userid = $info['User']['id'];

			$this->loadModel('Post');
			$this->loadModel('Like');

			$userinfo = $info['User'];
			$ownPosts = $info['Posts'];
			$followings = $info['Following'];

			$ownPostsIds = array();
			foreach ($ownPosts as $key => $post) {
				array_push($ownPostsIds, $post['id']);
			}

			$userinfo['follower_count'] = sizeof($info['Followers']);
			$userinfo['following_count'] = sizeof($followings);
			$userinfo['post_count'] = sizeof($ownPostsIds);
			$userinfo['like_count'] = $this->Like->getLikeCount($ownPostsIds);
			$userinfo['repost_count'] = $this->Post->getRepostCount($ownPostsIds);

			if ($userid !== $loggedId) {
				$this->loadModel('Follower');
				$userinfo['is_following'] = $this->Follower->isFollowing($userid,$loggedId);
			}

			return $userinfo;
		}

		protected function _preparePostsToShow($ids, $page = null) {
			if ($this->Auth->loggedIn()) {
				$loggedId = $this->Auth->user('id');
			} else {
				$loggedId = null;
			}

			$this->loadModel('Post');
			$options = $this->Post->getAllPosts($ids,true,$loggedId);
			$this->Paginator->settings = $options;

			if (!is_null($page)) {
				$this->Paginator->settings['page'] = $page;
			}

			try {
				$allposts = $this->Paginator->paginate('Post');
			} catch (NotFoundException $e) {
				$this->redirect(
					array(
						"controller" => $this->params['controller'],
						"action" => $this->params['action']
					)
				);
			}

			if (!empty($allposts)) {
				$allposts = $this->Post->attachReposts($allposts);
			}

			return $allposts;
		}

	}
?>
