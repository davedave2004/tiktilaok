<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
	public $actsAs = array('Containable');

    public function validateFile($file) {
		$errors = array();
		if (!$this->fileIsPicture($file)) {
			array_push($errors, "File is not a valid picture");
		}
		if (!$this->fileSizeIsGood($file)) {
			array_push($errors, "File is too large");
		}
		return $errors;
    }

	public function fileIsPicture($file) {
		$validMIMETypes = array("image/jpeg","image/gif","image/png");
		if (getimagesize($file['tmp_name'])) {
			return in_array($file['type'], $validMIMETypes);
		} else {
			return false;
		}
	}

	public function fileSizeIsGood($file) {
		$size = $file['size'];
		return $size < 2000000; //2mb
	}

	public function deleteImg($directory,$file) {
		$img = new File(
			WWW_ROOT.DS."img".DS.$directory.DS.$file,
			false,
			0777
		);

		if ($img->name() !== 'default_pic' && $img->exists()) {
			$img->delete();
		}
	}
}
