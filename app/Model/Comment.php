<?php
	class Comment extends AppModel {

		public $validate = array(
			'comment' => array(
				'length' => array(
					'rule' => array('maxLength', 140),
					'message' => 'Post should not exceed 140 characters',
					'allowEmpty' => false
				)
			)
		);

		public $belongsTo = array(
			'Post' => array(
	            'className' => 'Post',
	            'foreignKey' => 'post_id'
			),
			'User' => array(
	            'className' => 'User',
	            'foreignKey' => 'user_id'
			)
		);

		public function getComments($postid) {
			$options = array(
				'conditions' => array(
					'post_id' => $postid
				),
				'contain' => array(
					'User.full_name',
					'User.username',
					'User.profile_pic'
				),
				'order' => array(
					'id DESC'
				)
			);
			$comments = $this->find('all',$options);
			return $comments;
		}

		public function getCommentCount($postid) {
			$options = array(
				'conditions' => array(
					'post_id' => $postid
				),
				'contain' => false
			);
			$count = $this->find('count',$options);
			return $count;
		}




	}



?>
