<?php
	class Follower extends AppModel {

		public $belongsTo = array(
			'User' => array(
				'className' => 'User',
				'foreignKey' => 'user_id'
			)
		);


		/**
		 * Follow a user.
		 * @param  int/string $followThis The id of the user to be followed
		 * @param  int/string $loggedInUser The id of the user to currently logged in. This user will follow $followThis.
		 * @return array $result if followed or unfollowed, and the current following,follower counts of $followThis
		 */
		function follow($followThis,$loggedInUser) {
			//means not yet following
			if (!$this->isFollowing($followThis,$loggedInUser)) {
				$this->create();
				$data = array('user_id' => $followThis, 'follower_id' => $loggedInUser);
				$this->save($data);
				$result = 'followed';
			} else {
				$this->deleteAll(array('user_id' => $followThis,'follower_id' => $loggedInUser));
				$result = 'unfollowed';
			}

			$followerCount = $this->getFollowerCount($followThis);
			$followingCount = $this->getFollowingCount($followThis);
			return array("result" => $result,"fer" => $followerCount, "fing" => $followingCount);
		}

		/**
		 * Check if the second id/param is following the first id/param
		 * @param  int/string $toCheck The id of the user to be checked if followed
		 * @param  int/string $loggedInUser The id of the user to be check if is already following.
		 * @return bool true if is following otherwise false
		 */
		function isFollowing($toCheck,$loggedInUser = null) {
			if (is_null($loggedInUser)) {
				return false;
			}

			$isFollowing = $this->find(
				'first',
				array(
					'fields' => array('id'),
					'conditions' => array(
						'user_id' => $toCheck,
						'follower_id' => $loggedInUser
					)
				)
			);
			//means not yet following
			if (empty($isFollowing)) {
				return false;
			} else {
				return true;
			}
		}

		/**
		 * Get the user_ids of the followers of a certain user
		 * @param  int $userId The id of the user
		 * @return array $ids The array of the user_ids of the followers
		 */
		function getFollowers($userId) {
			$result = $this->find(
				'all',
				array(
					'fields' => array('follower_id'),
					'conditions' => array(
						'user_id' => $userId
					),
					'contain' => false
				)
			);

			$ids = array();
			if (!empty($result)) {
				foreach ($result as $key => $value) {
					array_push($ids, $value['Follower']['follower_id']);
				}
			}

			return $ids;
		}

		/**
		 * Get the current follower count of a user
		 * @param  int/string $userId The user to be checked
		 * @return array $followerCount count of followers
		 */
		function getFollowerCount($userId) {
			$followerCount = $this->find(
				'count',
				array(
					'conditions' => array(
						'user_id' => $userId
					),
					'contain' => false
				)
			);
			return $followerCount;
		}

		/**
		 * Get the user_ids a certain user is following
		 * @param  int $userId The id of the user
		 * @return array $ids The array of the user_ids of who you are following
		 */
		function getFollowing($userId) {
			$result = $this->find(
				'all',
				array(
					'fields' => array('user_id'),
					'conditions' => array(
						'follower_id' => $userId
					),
					'contain' => false
				)
			);

			$ids = array();
			if (!empty($result)) {
				foreach ($result as $key => $value) {
					array_push($ids, $value['Follower']['user_id']);
				}
			}
			return $ids;
		}

		/**
		 * Get the current following count of a user
		 * @param  int/string $userId The user to be checked
		 * @return array $followingCount count of followings
		 */
		function getFollowingCount($userId) {
			$followingCount = $this->find(
				'count',
				array(
					'conditions' => array(
						'follower_id' => $userId
					),
					'contain' => false
				)
			);
			return $followingCount;
		}
	}
?>
