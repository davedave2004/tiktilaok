<?php
	class Like extends AppModel {

	    public $belongsTo = array(
	        'Post' => array(
	            'className' => 'Post',
	            'foreignKey' => 'post_id',
	            'fields' => 'id'
	        ),
	        'User' => array(
	            'className' => 'User',
	            'foreignKey' => 'user_id',
	            'fields' => 'id'
	        ),
	    );

		function likePost($postid,$id) {
			$isLiked = $this->find(
				'first',
				array(
					'fields' => array('id'),
					'conditions' => array(
						'Like.post_id' => $postid,
						'Like.user_id' => $id
					)
				)
			);
			//means not yet following
			if (empty($isLiked)) {
				$this->create();
				$data = array('user_id' => $id, 'post_id' => $postid);
				$this->save($data);

			} else {
				$this->delete($isLiked['Like']['id']);
			}
			$likes = $this->getLikeCount($postid);
			return $likes;
		}

		function getLikeCount($postid) {
			if (empty($postid)) {
				return 0;
			}

			$likes = $this->find(
				'count',
				array(
					'conditions' => array(
						'post_id' => $postid
					),
					'contain' => false
				)
			);
			return $likes;
		}

		function getWhoLikedThisPost($postid) {
			$all = $this->find(
				'all',
				array(
					'conditions' => array(
						'post_id' => $postid
					),
					'fields' => array(
						'user_id'
					),
					'contain' => false
				)
			);

			$userIds = array();
			foreach ($all as $key => $like) {
				array_push($userIds,$like['Like']['user_id']);
			}
			return $userIds;
		}
	}
?>
