<?php
	class Post extends AppModel {
		public $validate = array(
			'content' => array(
				'maxLength' => array(
					'rule' => array('maxLength', 140),
					'message' => 'Posts cannont exceed 140 characters',
					'allowEmpty' => true,
					'last' => false
				),
				'atleastOne' => array(
					'rule' => array('checkIfEmpty'),
					'message' => 'Posts cant be empty',
					'last' => true
				)
			),
			'attached' => array(
				'filesize' => array(
					'rule' => array('validatePostFileSize'),
					'message' => 'File is too large. Max size is 2MB',
					'allowEmpty' => true,
					'last' => false
				),
				'filetype' => array(
					'rule' => array('validatePostFileType'),
					'message' => 'File is not a valid image.',
					'allowEmpty' => true,
					'last' => false
				),
				'atleastOne' => array(
					'rule' => array('checkIfEmpty'),
					'message' => 'Posts cant be empty',
					'last' => true
				)
			),
		);

		//Put Associations Here
	    public $belongsTo = array(
	        'User' => array(
	            'className' => 'User',
	            'foreignKey' => 'user_id'
	        )
	    );

	    public $hasMany = array(
	    	'Likes' => array(
	    		'className' => 'Like',
	    		'dependent' => 'true'
	    	),
	    	'Comments' => array(
	    		'className' => 'Comment',
	    		'dependent' => 'true'
	    	)
	    );

	    public function checkIfEmpty(){
		   if (empty($this->data[$this->alias]['content']) && $this->data[$this->alias]['attached']['error']==4) {
		   		return false;
		   } else {
				return true;
		   }
	    }

	    public function validatePostFileSize($attached){
	    	$file = $attached['attached'];
	    	if ($file['error'] == 4) {
	    		return true;
	    	}
	    	return $this->fileSizeIsGood($file);
	    }

	    public function validatePostFileType($attached){
	    	$file = $attached['attached'];
	    	if ($file['error'] == 4) {
	    		return true;
	    	}
	    	return $this->fileIsPicture($file);
	    }

		public function beforeSave($options = array()) {
			if (array_key_exists('attached', $this->data[$this->alias])) {
				$file = $this->data[$this->alias]['attached'];
				if ($file['error']===0) {
					$filetype = strtolower(pathinfo($file['name'],PATHINFO_EXTENSION));
					$newfilename = $this->data[$this->alias]['user_id'].date("ymdLHis").'.'.$filetype;
					$this->data[$this->alias]['picture'] = $newfilename;
				} else {
					$this->data[$this->alias]['picture'] = null;
				}
			}
		}

		public function afterSave($created,$options = array()) {
			if (array_key_exists('picture', $this->data[$this->alias])) {
				$filename = $this->data[$this->alias]['picture'];
				$file = $this->data[$this->alias]['attached'];
				if($file['error'] === 0){
					move_uploaded_file($file['tmp_name'],WWW_ROOT.DS."img".DS."attached".DS.$filename);
					if (isset($this->data[$this->alias]['oldpicture'])) {
						$oldpicture = $this->data[$this->alias]['oldpicture'];
						if (!empty($oldpicture)) {
							$this->deleteImg("attached",$oldpicture);
						}
					}
				}
			}
		}

		function beforeDelete($cascade = true){
		    $rowToBeDeleted = $this->find(
		    	"first",
		    	array(
		    		"conditions" => array(
		    			"id" => $this->id
		    		),
		    		'contain' => false
		    	)
		    );
		    if (!is_null($rowToBeDeleted['Post']['picture'])) {
		        $this->deletePicture = $rowToBeDeleted['Post']['picture'];
		    }
		}

		public function afterDelete() {
			if (isset($this->deletePicture)) {
				$this->deleteImg("attached",$this->deletePicture);
				unset($this->deletePicture);
			}
		}

		/**
		 * Get all the posts of the ids provided
		 * @param  array int $ids user_ids of posts to fetch. maybe single id or array
		 * @return array posts  all the posts of the ids provided
		 */
		function getAllPosts($ids,$paginate=false,$loggedId=null) {

			$this->virtualFields['like_count'] = 'Count(`Like`.`id`)';
			$this->virtualFields['editted'] = '`Post`.`created`<>`Post`.`modified`';

			$contain = array(
				'User.username',
				'User.full_name',
				'User.profile_pic',
				'User.id',
				'Comments.post_id'
			);

			if (!is_null($loggedId)) {
				$isLiked = 'Likes.user_id = '.$loggedId;
				array_push($contain, $isLiked);
			}

			$options =  array(
				'joins' => array(
					array(
						'table' => 'likes',
						'alias' => 'Like',
						'type' => 'left',
						'conditions' => array(
							'Like.post_id = Post.id'
						)
					)
				),
				'conditions' => array(
					'Post.user_id' => $ids
				),
				'fields' => array(
					'Post.id',
					'Post.content',
					'Post.picture',
					'Post.created',
					'Post.is_repost',
					'Post.reposted_post_id',
					'Post.like_count',
					'Post.editted'
				),
				'limit' => 10,
				'order' => 'Post.id DESC',
				'group' => array('Post.id'),
				'contain' => $contain
			);

			if ($paginate) {
				return $options;
			} else {
				$allposts = $this->find('all',$options);
				return $allposts;
			}
		}

		function attachReposts ($posts) {

			if (!array_key_exists('0', $posts)) {
				if ($posts['Post']['is_repost']) {
					$posts['Post']['Repost'] = $this->getRepost($posts['Post']['reposted_post_id']);
				}
				$posts['Post']['repost_count'] = $this->getRepostCount($posts['Post']['id']);
			} else {
				foreach ($posts as $key => $post) {
					if ($post['Post']['is_repost']) {
						$posts[$key]['Post']['Repost'] = $this->getRepost($post['Post']['reposted_post_id']);
					}
					$posts[$key]['Post']['repost_count'] = $this->getRepostCount($post['Post']['id']);
				}
			}

			return $posts;
		}

		function getRepost($postid) {
			$contain = array(
				'User.username',
				'User.full_name',
				'User.profile_pic',
				'User.id'
			);
			$options =  array(
				'conditions' => array(
					'Post.id' => $postid
				),
				'fields' => array(
					'Post.id',
					'Post.content',
					'Post.created',
				),
				'contain' => $contain
			);

			$postinfo = $this->find('first',$options);
			return $postinfo;
		}

		function getPost($id, $loggedId = null, $contain = true) {

			$options = $this->getAllPosts(null,true,$loggedId);
			$options['conditions'] = array('Post.id' => $id);
			if (!$contain) {
				$options['contain'] = false;
			}

			$postinfo = $this->find('first',$options);
			return $postinfo;
		}

		function getWhoRepostedThisPost($postid) {
			$all = $this->find(
				'all',
				array(
					'conditions' => array(
						'reposted_post_id' => $postid
					),
					'fields' => array(
						'user_id'
					),
					'contain' => false
				)
			);

			$userIds = array();
			foreach ($all as $key => $repost) {
				array_push($userIds,$repost['Post']['user_id']);
			}
			return $userIds;
		}

		function getRepostCount($postid) {

			if (empty($postid)) {
				return 0;
			}

			$repostCount = $this->find(
				'count',
				array(
					'conditions' => array(
						'reposted_post_id' => $postid
					)
				)
			);
			return $repostCount;
		}

		function getPostCount($userId) {
			$postCount = $this->find(
				'count',
				array(
					'conditions' => array(
						'user_id' => $userId
					)
				)
			);
			return $postCount;
		}

		function ifPostExists($postid){
			return $this->hasAny(array('id'=>$postid));
		}

	}
?>
