<?php
	App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
	App::uses('File', 'Utility');
	class User extends AppModel {
		public $validate = array(
			'username' => array(
				'alphaNumericWithSymbols' => array(
					'rule' => '/^[0-9a-z@.?!_]+$/i',
					'message' => 'Please use only letters, numbers, and @ . ? ! _',
					'last' => false
				),
				'isUnique' => array(
					'rule' => 'isUnique',
					'message' => 'That username is already taken',
					'last' => false
				),
				'maxLength' => array(
					'rule' => array('maxLength',255),
					'message' => 'Cannot exceed 255 characters',
					'last' => false
				),
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Please enter an email address'
				)
			),
			'email' => array(
				'validEmail' => array(
					'rule' => 'email',
					'message' => 'Please use a valid email address',
					'last' => false
				),
				'isUnique' => array(
					'rule' => 'isUnique',
					'message' => 'That email address is already taken',
					'last' => false
				),
				'maxLength' => array(
					'rule' => array('maxLength',255),
					'message' => 'Cannot exceed 255 characters',
					'last' => false
				),
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Please enter an email address'
				)
			),
			'password' => array(
				'minLength' => array(
					'rule' => array('minLength',8),
					'message' => 'Please use 8 characters or longer',
					'last' => false
				),
				'maxLength' => array(
					'rule' => array('maxLength',255),
					'message' => 'Cannot exceed 255 characters',
					'last' => false
				),
				'match' => array(
					'rule'=> array('confirmPassword'),
					'message' => 'Passwords must match'
				)
			),
			'oldpassword' => array(
				'matches' => array(
					'rule' => array('matchCurrentPassword'),
					'message' => 'Please enter current password',
					'on' => 'update'
				)
			),
			'full_name' => array(
				'alphaNumericWithSpace' => array(
					'rule' => '/^[a-z]*[a-z ]*[a-z]$/i',
					'message' => 'Please use only letters',
					'last' => false
				),
				'maxLength' => array(
					'rule' => array('maxLength',255),
					'message' => 'Cannot exceed 255 characters',
					'last' => false
				),
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Please enter an email address'
				)
			),
			'description' => array(
				'maxLength' => array(
					'rule' => array('maxLength',255),
					'message' => 'Cannot exceed 255 characters',
					'last' => false
				),
			),
			'picture' => array(
				'filesize' => array(
					'rule' => array('validateProfilePictureFileSize'),
					'message' => 'File is too large. Max size is 2MB',
					'allowEmpty' => true,
					'last' => false
				),
				'filetype' => array(
					'rule' => array('validateProfilePictureFileType'),
					'message' => 'File is not a valid image.',
					'allowEmpty' => true,
					'last' => true
				)
			),
		);

		//Put Associations Here
		public $hasMany = array(
	        'Posts' => array(
	            'className' => 'Post',
	            'order' => 'id DESC'
	        ),
	        'Followers' => array(
	            'className' => 'Follower',
	            'foreignKey' => 'user_id',

	        ),
	        'Following' => array(
	            'className' => 'Follower',
	            'foreignKey' => 'follower_id'
	        ),
	        'Likes' => array(
	        	'className' => 'Like',
	        )
	    );
		//pag home find all posts for this user
		//pag nasa profile find all posts by this user

	    public function validateProfilePictureFileSize(){
	    	$file = $this->data[$this->alias]['picture'];
	    	if ($file['error'] == 4) {
	    		return true;
	    	}
	    	return $this->fileSizeIsGood($file);
	    }

	    public function validateProfilePictureFileType(){
	    	$file = $this->data[$this->alias]['picture'];
	    	if ($file['error'] == 4) {
	    		return true;
	    	}
	    	return $this->fileIsPicture($file);
	    }

		public function confirmPassword(){
			return $this->data[$this->alias]['password'] === $this->data[$this->alias]['confirm_password'];
		}

		public function matchCurrentPassword() {
			$this->id = $this->data[$this->alias]['id'];

			$oldpassword = $this->data[$this->alias]['oldpassword'];

			$currentpass = $this->field('password');

			$passwordHasher = new BlowfishPasswordHasher();
			return $passwordHasher->check($oldpassword,$currentpass);
		}

	    public function beforeSave($options = array()) {
	        // password encryption using blowfish password hasher
	        if (isset($this->data[$this->alias]['password'])) {
	            $passwordHasher = new BlowfishPasswordHasher();
	            $this->data[$this->alias]['password'] = $passwordHasher->hash($this->data[$this->alias]['password']);
	        }

	        if (isset($this->data[$this->alias]['is_verified'])) {
	        	if (!$this->data[$this->alias]['is_verified']) {
	        		$this->data[$this->alias]['verification_token'] = md5(str_shuffle($this->data[$this->alias]['email']));
	        	}
	        }

	        return true;
	    }

	    public function updateProfilePicture($id,$file){

			$filetype = strtolower(pathinfo($file['name'],PATHINFO_EXTENSION));
			if ($file['error']===0) {
				$newfilename = $id.date("ymdLHis").'.'.$filetype;
				// = $newfilename;
				$oldpicture = $this->find('first',array(
					'conditions' => array(
						'id' => $id
					),
					'fieldList' => 'profile_pic',
					'contain' => false
				));
				$this->id = $id;
				if ($this->saveField('profile_pic',$newfilename)) {
					move_uploaded_file($file['tmp_name'],WWW_ROOT.DS."img".DS."profile".DS.$newfilename);

					$this->deleteImg("profile",$oldpicture['User']['profile_pic']);

					return true;
				} else {
					return false;
				}
			}
	    }


	    public function getIdByUsername($username) {
	    	$info = $this->find('first', array(
			  'conditions'=>array('username'=>$username),
			  'fields'=>array('id')
			));
			return $info['User']['id'];
	    }

	    public function getUserInfo($id) {

	    	$options = array(
				'fields' => array(
					'username',
					'full_name',
					'profile_pic',
					'description',
					'id'
				),
				'contain' => array(
					'Posts.id',
					'Followers.follower_id',
					'Following.user_id'
				)
			);

		    if (is_numeric($id)) {
		    	$options['conditions'] = array('id' => $id);
		    } else {
		    	$options['conditions'] = array('username' => $id);
		   	}

	    	$info = $this->find('first',$options);
			return $info;
	    }

	    public function getInfoByIds($ids,$paginate=false,$loggedIn=null) {

	    	if (is_null($loggedIn)) {
	    		$contain = false;
	    	} else {
	    		$contain = 'Followers.follower_id = '.$loggedIn;
	    	}

	    	$options = array(
				'conditions' => array(
					'id' => $ids
				),
				'fields' => array(
					'username',
					'full_name',
					'profile_pic',
					'description',
					'id',
					'created'
				),
				'order' => 'full_name ASC',
				'limit' => 10,
				'contain' => $contain
			);

			if ($paginate) {
				return $options;
			} else {
				$infos = $this->find('all',$options);
				return $infos;
			}

	    }

	    public function getSuggestionsFor($id,$following) {
	    	array_push($following, $id);
	    	$suggestions = $this->find(
	    		'all',
	    		array(
	    			'conditions' => array(
	    				'NOT' => array(
	    					'id' => $following
	    				)
	    			),
	    			'fields' => array(
						'username',
						'full_name',
						'profile_pic',
						'description',
						'id'
					),
					'limit' => 5,
					'order' => array('RAND()'),
	    			'contain' => false
	    		)
	    	);
	    	return $suggestions;
	    }

	    public function getVerificationTokenByUsername($username) {

	    	$info = $this->find(
				'first',
				array(
					'conditions' => array(
						'username' => $username
					),
					'fields' => array(
						'username',
						'verification_token',
						'email'
					),
					'contain' => false
				)
			);
			return $info;
	    }

	    public function verifyToken($user,$token=null) {
	    	if(is_null($token)) {
	    		return array("error" => true, "message" => "Invalid Token");
	    	}

	    	$record = $this->find(
	    		'first',
	    		array(
	    			'conditions' => array(
	    				'verification_token' => $token,
	    				'username' => $user
	    			)
	    		)
	    	);

	    	if(empty($record)) {
	    		//means token does not match
	    		return array("error" => true, "message" => "Invalid Token");
	    	} else {
	    		$id = $record['User']['id'];
				$this->id = $id;
	   			if ($this->saveField('is_verified',true)) {
	   				return array("error" => false,"user" => $record);
	   			} else {
	   				return array("error" => true,"message"=>"Could not Verify at the moment");
	   			}

	    	}
	    }
	}
?>

