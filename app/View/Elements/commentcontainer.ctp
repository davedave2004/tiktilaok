<?php
	$user = $commentinfo['User'];
	$comment = $commentinfo['Comment'];
?>

<div class="comment-container">

	<?php
		echo $this->Html->image("profile/".$user['profile_pic'],
			array(
				"class" => "post-profile-picture",
				"url" => array("controller" => "users","action"=>"profile",$user['username'])
			)
		);
	?>

	<div class="post-text">
		<span>
			<?php
		    	echo $this->Html->link(
        			$this->Html->tag('strong', $user['full_name'], array('class' => 'full-name'))
        			.$this->Html->tag('i','@'.$user['username'], array('class' => 'user-name')),
        			array(
        	 			'controller' => 'users',
             			'action' => 'profile',
             			$user['username']
   	 				),
        			array(
         				"escape" => false
        			)
        		);
			?>
			<span class="date right"><?php echo $this->Time->niceShort($comment['created']); ?></span>
		</span>

		<p class="content">
			<?php echo h($comment['comment']); ?>
		</p>
	</div>

</div>
