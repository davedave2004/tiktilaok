<div>
	<?php

		echo $this->Html->image(
			'profile/'.$this->request->data['User']['profile_pic'],
			array("class" => "edit-profile-picture")
		);

		echo $this->Form->create('User', array('type' => 'file'));
		echo $this->Form->input('id', array('type' => 'hidden'));
		echo $this->Form->input(
			'picture',
			array(
				'class' => 'upload-new-picture',
				'type' => 'file',
				'label' => false,
				'default' => ''
			)
		);
		echo $this->Form->end();
	?>

</div>
<?php
$this->assign('title',"Edit Profile");
echo $this->Form->create('User');
echo $this->Form->input('username',array('disabled'=>true));
echo $this->Form->input('email',array('disabled'=>true));
echo $this->Form->input('full_name');
echo $this->Form->input('description',array('class' => 'autoExpand', 'rows'=> 3, 'data-min-rows' => 3));
echo $this->Form->hidden('id');
echo $this->Form->end('Save');
?>
