<?php
echo $this->Form->create(
	'User',
	array(
		'inputDefaults' =>  array(
			'value' => ''
		)
	)
);

echo $this->Form->input('oldpassword',array('type'=>'password','label'=>'Current Password'));
echo $this->Form->input('password',array('type'=>'password'));
echo $this->Form->input('confirm_password',array('type'=>'password'));
echo $this->Form->hidden('id');
echo $this->Form->end('Save');
?>
