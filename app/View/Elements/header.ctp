<div id="header" class="header">
	<?php
		echo $this->Html->image(
			"logo/small-roostericon.png",
			array(
				"class" => "header-icon",
				"url" => array(
					"controller" => "users",
					 "action" => "login"
				)
			)
		);
		echo $this->Html->link(
			"Tiktilaok",
			array("controller" => "users", "action" => "index"),
			array("class" => "header-title")
		);
	?>
	<div class="right-header right">
		<?php
			if ($hideSearchBar === false) {
				echo $this->Form->create(
					'',
					array(
						'type' => 'get',
						"url" => array("controller" => "search", 'action' => 'index'),
						"class" => "search-form"
					)
				);
				echo $this->Form->input('kw', array("label" => false,"placeholder" => "Search"));
				echo $this->Form->end();
			}
		?>

		<?php if(isset($authUser)): //naka log in?>
			<button #id="header-quick-post-button" class="post-button show-quick-post-button">Post</button>
			<div class="profile-dropdown">
			  <button class="profile-button"> Profile </button>
			  <div class="dropdown-content">
				<?php
					echo $this->Html->link(
						"View Profile",
						array("controller" => "users", "action" => "profile", $authUser['username'])
					);
					echo $this->Html->link(
						"Edit Profile",
						array("controller" => "users", "action" => "edit")
					);
				?>
				<hr>
				<?php
					echo $this->Html->link(
						"Logout",
						array("controller" => "users", "action" => "logout")
					);
				?>
			  </div>
			</div>
		<?php endif; ?>
	</div>
</div>
<div class="header-margin"></div>
