<center>
	<?php
		echo $this->Html->image('profile/'.$data['profile_pic'],array(
			"class" => "home-profile-picture",
			"url" => array("controller" => "users","action"=>"profile",$data['username'])
		));
	?>
</center>
<div>
	<?php
		echo $this->Form->hidden("idforrefresh",array('value'=>$data['id']));
		echo $this->Html->link(
			h($data['full_name']),
			array("controller" => "users", "action" => "profile",$data['username']),
			array("class" => array("improvised-link"))
		);
	?>
	<br>
	<?php
		echo $this->Html->link(
			'@'.h($data['username']),
			array("controller" => "users", "action" => "profile",$data['username']),
			array("class" => array("improvised-link","user-name"))
		);
	?>
	<p class="description">
    <?php
    	if (is_null($data['description']) || empty($data['description'])) {
    		echo $this->Html->link("Add a description here", array("controller" => "users", "action" => "edit"));
    	} else {
    		echo nl2br(h($data['description']));
    	}
    ?>
	</p>

	<p class="stat-line">
		<a class="improvised-link" href=<?php echo $this->Html->url(array("controller" => "users","action" => "view", "followers",$data['username']));?>>
			<strong class="strong-label">Followers:</strong>
            <span class="label-text follower-count"><?php echo $data['follower_count']; ?></span>
		</a>
	</p>
	<p class="stat-line">
		<a class="improvised-link" href=<?php echo $this->Html->url(array("controller" => "users","action" => "view", "following",$data['username']));?>>
			<strong class="strong-label">Following:</strong>
            <span class="label-text following-count"><?php echo $data['following_count']; ?></span>
		</a>
	</p>
    <p class="stat-line">
      <strong class="strong-label">Post Count: </strong>
      <span class="label-text post-count"><?php echo $data['post_count']; ?></span>
    </p>
    <p class="stat-line">
      <strong class="strong-label">Like Count: </strong>
      <span class="label-text like-count"><?php echo $data['like_count']; ?></span>
    </p>
    <p class="stat-line">
      <strong class="strong-label">Repost Count: </strong>
      <span class="label-text repost-count"><?php echo $data['repost_count']; ?></span>
    </p>
</div>
