<center>
<?php



	if (isset($action)) {
		$route = array('action' => $action);
		if (!empty($params)) {
			foreach ($params as $key => $param) {
				array_push($route, $param);
			}
		}
		$this->Paginator->options['url'] = $route;
	}

	if ($this->Paginator->hasPrev()) {
		echo $this->Paginator->prev('« Previous ', null, null);
	}
	if ($this->Paginator->params()['pageCount'] > 1) {
		echo $this->Paginator->numbers();
	}
	if ($this->Paginator->hasNext()) {
		echo  $this->Paginator->next(' Next »', null, null);
	}
?>
</center>
