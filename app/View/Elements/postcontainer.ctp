<?php
	$post = $postinfo['Post'];
	$user = $postinfo['User'];
	if (isset($postinfo['Likes'])) {
		$isLiked = !empty($postinfo['Likes']);
	} else {
		$isLiked = false;
	}
?>
<div class="post" data-post="<?php echo $post['id']; ?>" data-repost="<?php echo $post['reposted_post_id'];?>">
	<?php
		echo $this->Html->image("profile/".$user['profile_pic'],
			array(
				"class" => "post-profile-picture",
				"url" => array("controller" => "users","action"=>"profile",$user['username'])
			)
		);
	?>
	<div class="post-text">
		<?php
		    echo $this->Html->link(
	            $this->Html->tag('strong', $user['full_name'], array('class' => 'full-name'))
	            .$this->Html->tag('i','@'.$user['username'], array('class' => 'user-name')),
	            array(
	                 'controller' => 'users',
	                 'action' => 'profile',
	                 $user['username']
	            ),
	            array(
	             	"escape" => false
	            )
	        );
		?>

		<p class="content"><?php echo nl2br(h($post['content'])); ?></p>

		<center>
		<?php
			if (!is_null($post['picture'])) {
				echo $this->Html->image("attached/".$post['picture'],array(
					"class" => "post-attached-picture"
				));
			}
		?>
		</center>

		<?php if (isset($post['Repost']) && !empty($post['Repost'])):
			$repost = $post['Repost']['Post'];
			$repostuser = $post['Repost']['User'];
		?>
	        <div class="reposted-post repost">
	            <div class="post-text repost">
					<?php
					    echo $this->Html->link(
				            $this->Html->tag('strong', $repostuser['full_name'], array('class' => 'full-name'))
				            .$this->Html->tag('i','@'.$repostuser['username'], array('class' => 'user-name')),
				            array(
				                 'controller' => 'users',
				                 'action' => 'profile',
				                 $repostuser['username']
				            ),
				            array(
				             	"escape" => false
				            )
				        );
					?>

                	<blockquote class="content repost">
                		<?php echo $repost['content']; ?>
            		</blockquote>
	            </div>
	        </div>
		<?php elseif ($post['is_repost']): ?>
	        <div class="reposted-post repost deleted-post">
	            <div class="post-text repost">
	                <span class="">
	                    <strong></strong>
	                    <i></i>
	                </span>
                    <blockquote class="content repost">This post has been removed or deleted or deleted or deleted or deleted or deleted or not or deleted</blockquote>
	            </div>
	        </div>
	    <?php endif; ?>

		<p>
			<?php echo $this->Time->niceShort($post['created']);?>
			<i>
			<?php
				if ($post['editted']) {
					echo "(Editted)";
				}
			?>
			</i>
		</p>

		<div class="post-footer">
			<div class="post-button-div">
				<button class="show-comment-button" title="Comment">

				</button><span class="show-comment count post-comment-count"><?php echo sizeof($postinfo['Comments']); ?></span>
			</div>
			<div class="post-button-div">
				<button class="like-button feed-button <?php if ($isLiked) { echo "liked";} if(!isset($authUser)) { echo 'disabled';} ?>" title="Like">

				</button>
				<a href=<?php echo $this->Html->url(array("controller" => "users","action" => "view",'liked',$post['id'])) ?>>
					<span class="count post-like-count"><?php echo $post['like_count']; ?></span>
				</a>
			</div>
			<div class="post-button-div">
				<button class="repost-button feed-button <?php if(!isset($authUser)) { echo 'disabled';} ?>" title="Repost">

				</button>
				<a href=<?php echo $this->Html->url(array("controller" => "users","action" => "view",'reposted',$post['id'])) ?>>
					<span class="count post-repost-count"><?php echo $post['repost_count']; ?></span>
				</a>
			</div>
		</div>
	</div>
        <!-- Post menu -->
    <?php if (isset($authUser) && strcmp($authUser['username'],$user['username'])==0) : ?>
      <div class="post-menu-dropdown right">
        <button class="post-menu-button">&dtrif;</button>
        <ul class="post-menu-content">
          <li class="edit-post-item">Edit</li>
          <li class="delete-post-item">Delete</li>
        </ul>
      </div>
    <?php endif; ?>
    <!-- End of post menu -->
</div>
