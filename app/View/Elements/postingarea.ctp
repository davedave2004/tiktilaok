<?php
	echo $this->Form->create("Post",array(
		"type" => "file",
		"class" => "post-form",
		"url" => array(
			"controller" => "posts",
			"action" => "post"
		),
		"inputDefaults" => array(
			"div" => false,
			"label" => false
		)
	));
	echo $this->Form->textarea(
		"content",
		array(
			"placeholder" => "What's on your mind?",
			"value"=>"",
			"type" => "text",
			'rows'=>2,
			'data-min-rows'=>2,
			"class" => array("post-textarea","autoExpand")
		)
	);

?>
<span>
<?php
	echo $this->Form->input("attached",array("value"=>"","type" => "file"));
	echo $this->Form->input("Post",array("type" => "submit","class" => "post-button right"));
?>
</span>
<?php echo $this->Form->end(); ?>

