<div class="profile-info">

	<?php
			echo $this->Html->image('profile/'.$user['profile_pic'],array(
				"class" => "profile-picture",
				"url" => array("controller" => "users","action"=>"profile",$user['username'])
			));
	?>

	<div class="profile-info-text">
		<?php
			echo $this->Form->hidden("idforrefresh",array('value'=>$user['username']));
		    echo $this->Html->link(
	            $this->Html->tag('strong', $user['full_name'], array('class' => 'full-name'))
	            .$this->Html->tag('i','@'.$user['username'], array('class' => 'user-name')),
	            array(
	                 'controller' => 'users',
	                 'action' => 'profile',
	                 $user['username']
	            ),
	            array(
	             	"escape" => false
	            )
	        );
		?>
		<p class="description">
			<?php echo h($user['description']);?>
		</p>
		<p class="stat-line">
			<a class="view-link" href=<?php echo $this->Html->url(array("controller" => "users","action" => "view", "followers",$user['username']));?>>
				<strong class="strong-label view-follow">Followers: </strong><span class="follower-count"><?php echo $user['follower_count']; ?></span>
			</a>
		</p>
		<p class="stat-line">
			<a class="view-link" href=<?php echo $this->Html->url(array("controller" => "users","action" => "view", "following",$user['username']));?>>
				<strong class="strong-label view-follow">Following: </strong><span class="following-count"><?php echo $user['following_count']; ?></span>
			</a>
		</p>
		<span class="profile-stat-span">
			<strong class="strong-label">Post Count: </strong><span class="post-count"><?php echo $user['post_count']; ?></span>
		</span>
		<span class="profile-stat-span">
			<strong class="strong-label">Like Count: </strong><span class="like-count"><?php echo $user['like_count']; ?></span>
		</span>
		<span class="profile-stat-span">
			<strong class="strong-label">Repost Count: </strong><span class="repost-count"><?php echo $user['repost_count']; ?></span>
		</span>
	</div>
	<!-- Follow Button -->
	<?php if(isset($authUser) && ($authUser['username'] !== $user['username'])): ?>
		<button class="follow-button right <?php if ($user['is_following']) {echo 'followed';}?>" data-user="<?php echo $user['id'] ?>"><?php if ($user['is_following']) {echo 'Unfollow';} else {echo 'Follow';}?></button>
	<?php endif; ?>
</div>

