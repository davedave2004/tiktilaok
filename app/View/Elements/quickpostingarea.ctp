<div class="modal-content">
	<div class="post-edit-picture">
		<h2>Current Picture:</h2>
  	<center>
    	<img class="attached-picture" src="" alt="Current Picture">
  	</center>
	</div>

	<?php
		echo $this->Form->create("Post",array(
			"type" => "file",
			"class" => "post-form",
			"url" => array(
				"controller" => "posts",
				"action" => "post"
			),
			"inputDefaults" => array(
				"div" => false,
				"label" => false
			)
		));
		echo $this->Form->textarea("content",array("value"=>"","type" => "text",'rows'=>2,'data-min-rows'=>2,"class" => array("post-textarea","autoExpand"),"placeholder" => "Say something about this..."));
	?>
	<span>
	<?php
		echo $this->Form->input("attached",array("value"=>"","type" => "file"));
		echo $this->Form->hidden('postid');
		echo $this->Form->hidden('oldpicture');
		echo $this->Form->input("Post",array("type"=>"submit", 'div'=> false , "class" => array("post-button","quick-post-modal-submit","right")));
	?>
	</span>
	<?php echo $this->Form->end(); ?>

</div>
