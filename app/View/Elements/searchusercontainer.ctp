<?php
	$profileinfo = $result['User'];
	$followed = !empty($result['Followers']);
?>
<div class="profile-info">

	<?php
			echo $this->Html->image('profile/'.$profileinfo['profile_pic'],array(
				"class" => "profile-picture",
				"url" => array("controller" => "users","action"=>"profile",$profileinfo['username'])
			));
	?>

	<div class="profile-info-text">
		<?php
			echo $this->Form->hidden("idforrefresh",array('value'=>$profileinfo['username']));
		    echo $this->Html->link(
	            $this->Html->tag('strong', $profileinfo['full_name'], array('class' => 'full-name'))
	            .$this->Html->tag('i','@'.$profileinfo['username'], array('class' => 'user-name')),
	            array(
	                 'controller' => 'users',
	                 'action' => 'profile',
	                 $profileinfo['username']
	            ),
	            array(
	             	"escape" => false
	            )
	        );
		?>
		<p class="search-description-p">
			<?php echo nl2br(h($profileinfo['description']));?>
		</p>
		<span>Member since <?php echo $this->Time->format($profileinfo['created'],'%B %e, %Y'); ?></span>
	</div>
	<!-- Follow Button -->
	<?php if(isset($authUser) && ($authUser['username'] !== $profileinfo['username'])): ?>
		<button class="follow-button right <?php if ($followed) {echo 'followed';}?>" data-user="<?php echo $profileinfo['id'] ?>"><?php if ($followed) {echo 'Unfollow';} else {echo 'Follow';}?></button>
	<?php endif; ?>
</div>
