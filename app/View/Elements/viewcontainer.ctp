<?php
	$view_user = $info['User'];
	$followed = false;

	if(isset($info['Followers'])) {
		$followed = !empty($info['Followers']);
	}
?>
<div class="view-container">
	<center>
	<?php
		echo $this->Html->image("profile/".$view_user['profile_pic'],
			array(
				"class" => "view-picture",
				"url" => array("controller" => "users","action"=>"profile",$view_user['username'])
			)
		);
		echo "<br>";
	    echo $this->Html->link(
            $this->Html->tag('strong', $view_user['full_name'], array('class' => 'full-name'))."<br>"
            .$this->Html->tag('i','@'.$view_user['username'], array('class' => 'user-name')),
            array(
                 'controller' => 'users',
                 'action' => 'profile',
                 $view_user['username']
            ),
            array(
            	"class" => "improvised-link",
             	"escape" => false
            )
        );
	?>
		<br>

	<?php if(isset($authUser) && ($authUser['username'] !== $view_user['username'])): ?>
		<button class="follow-button <?php if ($followed) {echo 'followed';}?>" data-user="<?php echo $view_user['id'] ?>"><?php if ($followed) {echo 'Unfollow';} else {echo 'Follow';}?></button>
	<?php endif; ?>

	</center>
</div>
