    <center>Thank you for registering on <strong>Tik-ti-laok</strong></center>
    <center>Activate your account by clicking the link below</center>
    <br>
    <center>
    	<?php echo $this->Html->link(
    		"Verify your account",
    		array(
    			"controller" => "users",
    			"action" => "verify",
    			"?"=> array(
    				"user" => $username,
    				"token" => $token
    			),
    			'full_base' => true
    		)
    	); ?>
    </center>
