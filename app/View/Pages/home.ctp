<?php echo $this->Html->css('index'); ?>
<center>

<?php

	echo $this->Html->image(
		'logo/small-roostericon.png',
		array(
			'class' => array(
				'middle-rooster'
			),
			'url' => array(
				'controller' => 'users',
				'action' => 'login'
			)
		)
	);
?>

<p class="click-it">Click the rooster</p>

</center>

<script type="text/javascript">
	document.querySelector(".click-it").addEventListener('mouseover',function(){
		this.innerHTML = "Come on, click it!";
	});
	document.querySelector(".click-it").addEventListener('mouseout',function(){
		this.innerHTML = "Click the rooster";
	});
</script>
