<?php
	$post = $postinfo['Post'];
	$user = $postinfo['User'];
	if (isset($postinfo['Likes'])) {
		$isLiked = !empty($postinfo['Likes']);
	} else {
		$isLiked = false;
	}
	$comments = $postinfo['Comments'];
?>
 <div class="modal">
	<div class="modal-content">
		<center>
			<div class="modal-post post" data-post="<?php echo $post['id']; ?>" data-repost="<?php echo $post['reposted_post_id']; ?>">

				<?php
					echo $this->Html->image("profile/".$user['profile_pic'],
						array(
							"class" => "post-profile-picture",
							"url" => array("controller" => "users","action"=>"profile",$user['username'])
						)
					);
				?>

				<div class="post-text">
					<span>
						<?php
					    	echo $this->Html->link(
		            			$this->Html->tag('strong', $user['full_name'], array('class' => 'full-name'))
		            			.$this->Html->tag('i','@'.$user['username'], array('class' => 'user-name')),
		            			array(
	                	 			'controller' => 'users',
		                 			'action' => 'profile',
		                 			$user['username']
	           	 				),
		            			array(
		             				"escape" => false
		            			)
			        		);
						?>

						<span class="date right">
							<i><?php if ($post['editted']) { echo "(Editted)"; } ?></i>
							<?php echo $this->Time->niceShort($post['created']); ?>
						</span>
					</span>

					<p class="content">
						<?php echo h($post['content']); ?>
					</p>

					<center>
					<?php
						if (!is_null($post['picture'])) {
							echo $this->Html->image("attached/".$post['picture'],array(
								"class" => "attached-picture"
							));
						}
					?>
					</center>

					<?php if (isset($post['Repost']) && !empty($post['Repost'])):
						$repost = $post['Repost']['Post'];
						$repostuser = $post['Repost']['User'];
					?>
				        <div class="reposted-post repost">
				            <div class="post-text repost">
								<?php
								    echo $this->Html->link(
							            $this->Html->tag('strong', $repostuser['full_name'], array('class' => 'full-name'))
							            .$this->Html->tag('i','@'.$repostuser['username'], array('class' => 'user-name')),
							            array(
							                 'controller' => 'users',
							                 'action' => 'profile',
							                 $repostuser['username']
							            ),
							            array(
							             	"escape" => false
							            )
							        );
								?>

			                	<blockquote class="content repost">
			                		<?php echo $repost['content']; ?>
			            		</blockquote>
				            </div>
				        </div>
					<?php elseif ($post['is_repost']): ?>
				        <div class="reposted-post repost deleted-post">
				            <div class="post-text repost">
				                <span class="">
				                    <strong></strong>
				                    <i></i>
				                </span>
			                    <blockquote class="content repost">This post has been removed or deleted or deleted or deleted or deleted or deleted or not or deleted</blockquote>
				            </div>
				        </div>
				    <?php endif; ?>
				</div>
			</div>

			<div class="post-footer">
				<div class="post-button-div">
					<button class="show-comment-button" title="Comment">

					</button><span class="show-comment count post-comment-count"><?php echo sizeof($postinfo['Comments']); ?></span>
				</div>
				<div class="post-button-div">
					<button class="like-button <?php if ($isLiked) { echo "liked";} if(!isset($authUser)) { echo 'disabled';} ?>" title="Like">

					</button>
					<a href=<?php echo $this->Html->url(array("controller" => "users","action" => "view",'liked',$post['id'])) ?>>
					<span class="count post-like-count"><?php echo $post['like_count']; ?></span>
				</a>
				</div>
				<div class="post-button-div">
					<button class="repost-button <?php if(!isset($authUser)) { echo 'disabled';} ?>" title="Repost">

					</button><span class="count post-repost-count"><?php echo $post['repost_count']; ?></span>
				</div>
			</div>

			<!-- For Comments -->
			<div class="modal-postarea">
				<textarea name="commentarea" class="post-textarea autoExpand" rows='2' data-min-rows='2'  placeholder="Add a comment" maxlength="140"></textarea>
				<button data-post class="post-button  comment-button <?php if(!isset($authUser)) { echo 'disabled';} ?>">Post</button>
			</div>
			<div class="comment-section">

				<?php
					foreach ($comments as $key => $comment) {
						echo $this->element('commentcontainer',array('commentinfo' => $comment));
					}
				?>

			</div>
		</center>
	</div>
</div>

<script type="text/javascript">
	loadModalNeeds();
</script>
