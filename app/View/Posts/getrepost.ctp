<?php
	$post = $postinfo['Post'];
	$user = $postinfo['User'];
?>
<div class="higher-modal">
	<div class="modal-content">
		<center>
			<h2 class="modal-header">Repost to followers</h2>
			<div class="modal-post post">
				<?php
					echo $this->Html->image("profile/".$user['profile_pic'],
						array(
							"class" => "post-profile-picture",
							"url" => array("controller" => "users","action"=>"profile",$user['username'])
						)
					);
				?>
				<div class="post-text">
					<span>
						<?php
					    echo $this->Html->link(
				            $this->Html->tag('strong', $user['full_name'], array('class' => 'full-name'))
				            .$this->Html->tag('i','@'.$user['username'], array('class' => 'user-name')),
				            array(
				                 'controller' => 'users',
				                 'action' => 'profile',
				                 $user['username']
				            ),
				            array(
				             	"escape" => false
				            )
					        );
						?>
						<span class="date right"><?php echo $this->Time->niceShort($post['created']); ?></span>
					</span>
					<p class="content">
						<?php echo h($post['content']); ?>
					</p>
				</div>
			</div>
			<div class="modal-postarea">
				<textarea class="post-textarea autoExpand" rows='2' data-min-rows='2' placeholder="Say something about this" maxlength="140"></textarea>
				<button data-post="<?php echo $post['id'] ?>" class="post-button">Repost</button>
			</div>
		</center>
	</div>
</div>

<script type="text/javascript">
	$("#modal-repost .post-button").click(repost);
	prepareTextArea($(".modal autoExpand"));
</script>
