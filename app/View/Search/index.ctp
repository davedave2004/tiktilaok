<?php
	$this->assign('title',"Search on Tiktilaok");
	echo $this->Html->css('search');
?>
<center>
<?php
    echo $this->Form->create('', array('type' => 'get','class'=>'search-form'));
    echo $this->Form->input('kw', array("label" => false,'placeholder' => 'Search something...'));
    echo $this->Form->button('Search',array('class' => array('search-button')));
    echo $this->Form->end();
?>
</center>
