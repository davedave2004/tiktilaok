<?php
	$title = 'Search: '.h($keyword);
	$this->assign('title',$title);
	echo $this->Html->css('search');
?>

<div class="search-result-keyword">
	<h2>Search Results for:
		<i class="search-keyword"><?php echo h($keyword);?></i>
	</h2>
</div>
<div class="search-type-div">
	<?php
    echo $this->Form->create(
    	'',
    	array(
	    	'type' => 'get',
			"inputDefaults" => array(
				"div" => false,
				"label" => false
			)
	    )
	);
    echo $this->Form->input(
    	'kw',
    	array(
    		'type' => 'hidden',
    		'value' => $keyword
    	)
    );
    echo $this->Form->input(
    	'Users',
    	array(
    		'type' => 'button',
    		'name' => 'find',
    		'value' => 'users',
    		'class' => $user_class
    	)
    );
    echo $this->Form->input(
    	'Posts',
    	array(
    		'type' => 'button',
    		'name' => 'find',
    		'value' => 'posts',
    		'class' => $post_class
    	)
    );

    echo $this->Form->end();
    switch ($category) {
    	case 'posts':
    		echo $this->Html->css('post');
    		$container = 'postcontainer';
    		$variable = 'postinfo';
    		break;

    	default:
	    	echo $this->Html->css('profile');
    		$container = 'searchusercontainer';
    		$variable = 'result';
    		break;
    }
	?>
</div>

<div id="search-results">
	<?php
		foreach ($results as $key => $result) {
	    	echo $this->element($container,array($variable => $result));
	    }
	 ?>
</div>
<center>
<!-- Pagination: Display only when pages exceed 1 -->
<div class="pagination-container">

	<?php echo $this->element('mypaginator'); ?>

</div>
<!-- End Of Pagination -->
</center>
