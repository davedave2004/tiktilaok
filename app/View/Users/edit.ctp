<?php
	echo $this->Html->css('edit');
	$this->assign('title',"Edit Profile");
?>
<h1>Edit Profile</h1>

<span>
<?php
	echo $this->Html->link("General", array('controller' => 'users','action'=> 'edit', 'general'), array( 'class' => 'link-button'));
	echo $this->Html->link("Password", array('controller' => 'users','action'=> 'edit', 'password'), array( 'class' => 'link-button'));
?>
</span>

<?php
	switch ($category) {
		case 'password':
			echo $this->element('editpassword');
			break;

		default:
			echo $this->element('editgeneral');
			break;
	}
?>
