<?php
		$this->assign('title',"Tiktilaok");
		echo $this->Html->css(array('home','post','view'));
		echo $this->Flash->render('auth');
		echo $this->Flash->render();
?>
<div class="home-div">
	<!-- Home Information -->
	<div class="home-info-div">
		<?php	echo $this->element('homeinfo',array($data)); ?>
	</div>
	<!-- End of Home Info -->

	<!-- Posts Div -->
	<div class="home-post-div">
		<!-- Posting area -->
		<?php echo $this->element('postingarea') ?>
		<!-- Posting Area -->

		<!-- Feed -->
		<div id="postfeed" class="postfeed">

			<?php
				if (array_key_exists('0', $allposts)) {
					foreach ($allposts as $key => $postinfo) {
						echo $this->element('postcontainer', array("postinfo" => $postinfo));
					}
				}
			?>

			<!-- Pagination: -->
			<div class="pagination-container">

				<?php echo $this->element('mypaginator'); ?>

			</div>
			<!-- End Of Pagination -->
		</div>
		<!-- End of Feed-->



	</div>
	<!-- End of Posts Div -->

	<!-- Suggestions -->
	<section class="suggestions">
		<button id="refreshSuggestions">Refresh Suggestions</button>
		<div id="suggestions">
			<?php
				foreach ($suggestions as $key => $value) {
					echo $this->element('viewcontainer',array('info' => $value));
				}
			?>
		</div>
	</section>
	<!-- End of Suggestions -->
</div>
