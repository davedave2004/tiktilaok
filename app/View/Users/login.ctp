<?php
	$this->assign('title',"Tiktilaok");
	echo $this->Html->css('login');
?>
<div class="login-div">
	<div class="users form">
	<?php echo $this->Flash->render(); ?>
	<?php echo $this->Flash->render('auth'); ?>
	<?php echo $this->Form->create('User'); ?>
		<fieldset>
			<legend>
				<?php echo __('Tiktilaok'); ?>
			</legend>
			<?php echo $this->Form->input('username');
			echo $this->Form->input('password',array("value"=>""));
		?>
		</fieldset>
	<?php echo $this->Form->end(__('Login')); ?>
	<?php
		echo $this->Html->link("Register Here",array('action'=>'register'));
		echo " or ";
		echo $this->Html->link("Search Here",array('controller' => 'search', 'action'=>'index'));

	?>

	</div>
	<div class="login-right-div">
		<center>
			<?php
				echo $this->Html->image(
					"logo/redroostericon.png",
					array(
						"class" => array(
							"login-icon"
						)
					)
				);
			?>
		</center>
	</div>
</div>

