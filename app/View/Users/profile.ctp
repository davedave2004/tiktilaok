<?php
	$full_name = $user['full_name'];
	$username = $user['username'];
	$this->assign('title',"$full_name @$username");

	echo $this->Html->css('post');
	echo $this->Html->css('profile');
	echo $this->element('profilecontainer',array('user'=> $user));

?>

<div id="postfeed" class="postfeed">
	<?php

	if (array_key_exists('0', $allposts)) {
		foreach ($allposts as $key => $postinfo) {
			echo $this->element('postcontainer', array( 'postinfo' => $postinfo));
		}
	}

	?>
	<!-- Pagination: -->
	<div class="pagination-container">

		<?php echo $this->element('mypaginator'); ?>

	</div>
	<!-- End Of Pagination -->
</div>



