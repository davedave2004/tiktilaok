<?php
	$this->assign('title',$message);
	echo $this->Html->css('view');
?>
<h2><?php echo $message ?><span class="right"><?php echo $this->Paginator->params()['count']; ?></span></h2>

<div class="views-container">
	<?php
		foreach ($infos as $key => $info) {
			echo $this->element('viewcontainer',array('info'=>$info));
		}
	?>
</div>

<div class="pagination-container">

	<?php echo $this->element('mypaginator'); ?>

</div>
