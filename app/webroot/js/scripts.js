function loadUINeeds(){

    //Elements in header
    $(".profile-button").click(showProfileDropdown);
	$(".search-form").submit(checkIfSearchIsEmpty);
	$(".show-quick-post-button").click(showQuickPostModal);
	$(".quick-post-modal-submit").click(submitPost);

	//Elements can be in index,profile,search
	$(".follow-button").click(userFollow);

	//Element in edit
	$(".upload-new-picture").change(uploadProfilePicture);

	//Element in home/index
	$("#refreshSuggestions").click(refreshSuggestions);

	prepareTextArea($(".autoExpand"));

	//*/HERE:: Remove this line to try without javascript validation, although for submit only
	//maybe i needs to be on input of the file ?
	//$(".home-post-div > .post-form").submit(submitHomePost);

	loadPostNeeds();

    window.onclick = function(e) {
        //dropdown onclicks
        if (!e.target.matches(".post-menu-button,.profile-button")) {
           	closeDropdowns();
        }
       	if ($(e.target).parent().is("#modal-post")) {
       		closePostModal();
       		//refresh ung modal na binuksan
        }
        if ($(e.target).parent().is("#modal-repost")) {
       		closeRepostModal();
       		//refresh ung modal na binuksan
        }
        if ($(e.target).is("#modal-quick-post")) {
       		closeQuickPostModal();
       		//refresh ung modal na binuksan
        }
    }
}


function loadPostNeeds() {
	$(".liked").css('background-color','#d30313');
	$(".like-button.feed-button").click(postLike);
	$(".post-menu-button").click(showPostMenu);
	$(".edit-post-item").click(showEditPost);
	$(".delete-post-item").click(showDeletePost);
	$(".comment-button").click(postComment);
	$(".repost-button.feed-button").click(showRepostModal);
	$(".post").click(showPostModal);
}

function loadModalNeeds(){
	$(".modal .liked").css('background-color','#d30313');
	$(".modal .like-button").click(postLike);
	$(".modal .show-comment-button").click(function(){
			$(this).parents(".modal").find("textarea").focus();
		});
	$(".comment-button").click(postComment);
	$(".modal .repost-button").click(showRepostModal);
	$(".modal .reposted-post").click(
		function() {
			if ($(this).is(".deleted-post")) {
				unableToFindPost();
			} else {
				const postToShow = $(".modal .post").data("repost");
				showPost(postToShow);
			}

		}
	);
	prepareTextArea($(".modal .autoExpand"));
}

function findPostFromFeed(postid) {
	const posts = $("#search-results .post,.postfeed .post").filter(
		function(index,element){
			return $(element).data("post") == postid;
		}
	);
	return posts;
}

function showPostModal(e) {
	const post = this;

    if (!e.target.matches("button,strong,i,li") || e.target.matches(".show-comment-button")) {
        //if it is has no action, show post;
        var postToShow = post.dataset.post;
        if (e.target.matches(".repost")) {
            postToShow = post.dataset.repost;
        }
		showPost(postToShow);
    }
}

function showPost(postToShow) {

	var request = $("#modal-post").load(`/tiktilaok/posts/getpost/${postToShow}`,
		function( response, status, xhr ) {

			if ( status == "error" ) {
				console.error(xhr.status + " " + xhr.statusText );
				return;
			}

			if (response.trim() === "404") {
				closePostModal();
				unableToFindPost();
			} else {
				$("#modal-post").addClass("opened");
				hideScroll(true);
			}

		}
	);
}



function showRepostModal() {

	const button = $(this);
	var postId = $(button).parents(".post").data("post"); //postid


	if($(button).parents().is(".modal-content")) {
		postId = $(".modal-content .post").data("post");
	}

	var request = $("#modal-repost").load(`/tiktilaok/posts/getrepost/${postId}`,
		function( response, status, xhr ) {

			if ( status == "error" ) {
				if(xhr.status === 403) {
					alert("You are not logged in!");
				}
				return;
			}

			if (response.trim() === "404") {
				closeRepostModal();
				unableToFindPost();
			} else {
				hideScroll(true);
			}

		}
	);
}

function repost(){

	const repostpostid = $(this).data('post');
	const content = $(this).siblings("textarea").val();

	var request = $.post(
		"/tiktilaok/posts/repost.json",
		{
			'repostpostid' : repostpostid,
			'content' : content
		},
		function(data) {

			if (data.result === 1) {
				alert("reposted!");

				$("#modal-post").find(".post-repost-count").html(data.count);

				if ($("#search-results")) {
					const postInFeed = findPostFromFeed(repostpostid);
					$(postInFeed).find(".post-repost-count").html(data.count);
				}

				refreshPostFeed($("#idforrefresh").val());
	    		refreshProfileStats();
				closeRepostModal();
			}

		}
	).fail(function(error){console.error(error)});
}

function postComment() {
	const postid = $(this).parent().siblings(".post").data("post");
	const commentText = $(this).siblings("textarea").val();

	var request = $.post(
		"/tiktilaok/posts/comment.json",
		{
			'postid' : postid,
			'comment' : commentText
		},
		function(data) {

			if ((typeof data === 'string') && data.trim() === "404") {
				unableToFindPost();
				return;
			}

			if (data.result === 1) {
				updateCommentCount(findPostFromFeed(postid),data.count)
				showPost(postid);
				alert("commented!");
			}
		}//TODO::COMMENT
	).fail(function(error) {
		if (error.status === 403) {
			alert("You are not logged in!");
		}
	});
}

function showQuickPostModal() {
	const modal = $("#modal-quick-post");

	$(modal).find("textarea").val('');
	$(modal).find("#PostAttached").val('').toggle(true);
	$(modal).find("#PostPostid").val('');

	showEditPicture(false);
	$(modal).toggle(true);
	hideScroll(true);
}

function submitHomePost() {
	const errors = validateForm(this);
	if (errors.length != 0) {
		alert(errors);
		return false;
	} else {
		return true;
	}
}

function submitPost(e){
	e.preventDefault();

	const postForm = $("#modal-quick-post form")[0];
	const errors = validateForm(postForm);

	//*/HERE:: i skip ung error checking para ma check kung wala yung javascript
	if (errors.length != 0) {
		alert(errors);
		$(postForm).find("input[type=file]").val('');
		return;
	} else {
		const formData = new FormData(postForm);
		jQuery.ajax({
		    url: $(postForm).attr('action'),
		    data: formData,
		    cache: false,
		    contentType: false,
		    processData: false,
		    method: 'POST',
		    success: function(data){
		    	const result = data.trim();

		    	if (result === '409') {
		    		alert(`Something's wrong. What did you do?`);
		    	}

		    	if (result === "Posted") {
		    		alert(result);
		    		const postid = $(postForm).find("#PostPostid").val();
		    		//if it was an edit
		    		if (postid) {
		    			if ($("#search-results").length) {
		    				refreshPostContent(postid);
		    			}
		    		}

		    		refreshPostFeed($("#idforrefresh").val());

		    		refreshProfileStats();
		    		closeQuickPostModal();
		    	}
		    }
		});
	}
}




function showEditPost() {
	const modal = $("#modal-quick-post");
	const post = $(this).parents(".post");

	if ($(post).data("repost")) {
		$(modal).find("input[type=file]").toggle(false);
	} else {
		$(modal).find("input[type=file]").toggle(true);
	}

	const content = $(post).find("p.content")[0];
	const postpic = $(post).find("img.post-attached-picture")[0];

	$(modal).find("input[type=file]").val('');
	$(modal).find("#PostPostid").val($(post).data('post'));

	if (postpic) {
		const oldpicture = postpic.src.split(/[\\/]/).pop();
		$("#modal-quick-post .attached-picture").attr("src",`/tiktilaok/img/attached/${oldpicture}`);
		$(modal).find("#PostOldpicture").val(oldpicture);
		showEditPicture(true);
	} else {
		showEditPicture(false);
	}

	hideScroll(true);

	$(modal).toggle(true);
	$(modal).find("textarea").focus().val($(content).text()).trigger("input");
}

function showEditPicture(show) {
	if (show) {
		$("#modal-quick-post .post-edit-picture").toggle(true);
	} else {
		$("#modal-quick-post .attached-picture").attr("src",'');
		$("#modal-quick-post .post-edit-picture").toggle(false);
		$("#modal-quick-post #PostOldpicture").val('');
	}
}

function showDeletePost() {
	const item = $(this);

	if (confirm("Delete this post?")) {
		const postid = $(item).parents(".post").data("post");
		var url = `/tiktilaok/posts/delete/${postid}`;
		var request = $.post(
			url,
			function(data) {
				const result = data.trim();

		    	if (result === '409') {
		    		alert(`Something's wrong. What did you do?`);
		    	}

		    	if (result === 'deleted') {
					refreshPostFeed($("#idforrefresh").val());
					const postInFeed = findPostFromFeed(postid);
					$(postInFeed).remove();
					refreshProfileStats();
		    	}

			}
		).fail(function(error){console.error(error)});
	}
}

function postLike(){
	const button = $(this);
	var postId = $(button).parents(".post").data("post"); //postid
	var likedInModal = false;

	if($(button).parents().is("#modal-post")) {
		postId = $("#modal-post .post").data("post");
		likedInModal = true;
	}

	var request = $.post(
		"/tiktilaok/posts/like.json",
		{ 'likeThis' : postId },
		function(data) {

			if ((typeof data === 'string') && data.trim() === "404") {
				unableToFindPost();
				return;
			}

			const count = data.result;
			const postInFeed = findPostFromFeed(postId);

			updateLikeButton(button,count);

			if (likedInModal) {
				//update din ung nasa feed;
				const buttonInFeed = $(postInFeed).find(".like-button");
				updateLikeButton(buttonInFeed,count);
			}
			refreshProfileStats();
		}
	).fail(function(error) {
		if (error.status === 403) {
			alert("You are not logged in!");
		}
	});
}

function updateLikeButton(button,count) {
	$(button).toggleClass("liked");
	if ($(button).hasClass("liked")) {
		$(button).css('background-color','#d30313');
	} else {
		$(button).css('background-color','transparent');
	}

	$(button).siblings("a").children(".count").html(count);
}
function updateCommentCount(post,count) {
	$(post).find(".post-comment-count").html(count);
}


function userFollow(){
	const button = $(this);
	const userId = $(this).data("user");
	var request = $.post(
		"/tiktilaok/users/follow.json",
		{ 'followThis' : userId },
		function(data) {

			if (data.result == 'followed') {
				$(button).html("Unfollow").addClass("followed");
			} else {

				$(button).html("Follow").removeClass("followed");
			}

			if ($(".home-info-div")) {
				refreshProfileStats();
			} else {
				$(".follower-count").html(data.follower_count);
				$(".following-count").html(data.following_count);
			}
		}
	).fail(function(error){
		if (error.status === 403) {
			alert("You are not logged in!");
		}
		console.error(error);
	});
}


function refreshPostFeed(id){

	var url = "/tiktilaok/users/refreshfeed";

	var currentURL = getCurrentURL();

	var currentPage = $("span.current").text();

	var request = $("#postfeed").load(
		url,
		{
			'id' : id,
			'page' : currentPage,
			'action' : currentURL.action,
			'params' : currentURL.params
		},
		function( response, status, xhr ) {

			if ( status == "error" ) {
				console.error(xhr.status + " " + xhr.statusText );
			}
		}
	);

}

function refreshSuggestions() {
	console.log("refreshing");
	var request = $("#suggestions").load("/tiktilaok/users/getsuggestions/",
		function( response, status, xhr ) {
			if ( status == "error" ) {
				console.error(xhr.status + " " + xhr.statusText );
			}
		}
	);
}

function validateForm(form) {

    var errorMessages = [];

	const content = $(form).find("textarea").val();
	const file = $(form).find("input[type=file]")[0].files[0];

	if (!content && !file) {
		errorMessages.push("Post can't be empty");
	}

	if (file) {
		const result = validateFile(file);
		result.forEach(error => errorMessages.push(error));
		if (errorMessages.length != 0) {
			$(form).find("input[type=file]").val('');
		}
	}

	if (content.length >= 140) {
		errorMessages.push("Posts should not exceed 140 characters");
	}

	return errorMessages;
}

function uploadProfilePicture() {
    path = this.value;
    pattern = /[\\/]/;
    basename = path.split(pattern).pop();

    var errorMessages = [];

    if (this.files[0]) {
   		const file = this.files[0];
    	const result = validateFile(file);
    	result.forEach(error => errorMessages.push(error));
    }

    //*/HERE:: Remove this block to check validation without javascript
    if (errorMessages.length != 0) {
    	this.value="";
    	alert(errorMessages);
    	return false;
    }

    if (confirm(`Change profile pic to ${basename.split(".")[0]}?`)) {
    	$(this).parents("form").submit();
    } else {
    	this.value="";
    }
}

function validateFile(file) {

    var errors = [];
    var validFileTypes = ["jpg","jpeg","png","gif"];

    var filetype = file.name.split(".").pop().toLowerCase();

    if (file.size > 2000000) {
        errors.push("File is too large");
    }

    if (!validFileTypes.includes(filetype)) {
        errors.push("File is not a valid picture");
    }
    return errors;
}

function checkIfSearchIsEmpty() {
    const NotEmpty = $(".search-form input:text").val().trim();
    if (NotEmpty) {
    	return true;
    } else {
    	return false;
    }
}

function showProfileDropdown() {
    const dropdown = document.querySelector(".dropdown-content");
    if (!dropdown.classList.contains("show")) {
        closeDropdowns();
   	}
    dropdown.classList.toggle("show");
    if (!dropdown.classList.contains("show")) {
        this.blur();
    }
}

function showPostMenu() {

    const dropdown = this.parentElement.querySelector(".post-menu-content");
    if (!dropdown.classList.contains("show")) {
        closeDropdowns();
    }

    dropdown.classList.toggle("show");
    if (!dropdown.classList.contains("show")) {
        this.blur();
    }
}


function closeDropdowns() {
	$(".show").removeClass("show");
}
function closePostModal() {
	$("#modal-post").html('');
	$("#modal-post").removeClass("opened");
	hideScroll(false);
}
function closeRepostModal() {
	$("#modal-repost").html('');

	if (!$("#modal-post").is(".opened")) {
		hideScroll(false);
	}



}
function closeQuickPostModal() {
	$("#modal-quick-post").toggle(false);
	$("#modal-quick-post").find("input[name=postid]").val('');
	$("#modal-quick-post").find("textarea").val('').attr('rows',2);
	hideScroll(false);
}
function hideScroll(state) {
	if (state){
		$(document.body).addClass("modal-open");
	} else {
		$(document.body).removeClass("modal-open");
	}
}


function refreshProfileStats() {
	var userId = $("#idforrefresh").val();

	if (!userId) {
		return;
	}

	var request = $.post(
		"/tiktilaok/users/getprofilestats.json",
		{ 'id' : userId },
		function(data) {

			if (data) {
				$(".post-count").html(data.post_count);
				$(".repost-count").html(data.repost_count);
				$(".like-count").html(data.like_count);
				$(".follower-count").html(data.follower_count);
				$(".following-count").html(data.following_count);
			}
		}
	).fail(function(error){console.error(error)});
}


function prepareTextArea(element) {
	$(element)
    .one('focus', function(){
        var savedValue = this.value;
        this.value = '';
        this.baseScrollHeight = this.scrollHeight;
        this.value = savedValue;
    })
    .on('input', function(){
        var minRows = this.getAttribute('data-min-rows')|0, rows;
        //var minRows = 2;
        this.rows = minRows;
        rows = Math.ceil((this.scrollHeight - this.baseScrollHeight) / 16);
        this.rows = minRows + rows;
    });
}

function unableToFindPost() {
	alert('Something is wrong with the post. It may already be removed or deleted');
}

function getCurrentURL() {
	const mainURL = window.location.pathname;
	const vars = mainURL.split('/');

	const completeInfo = {
		url:mainURL,
		host:vars[1],
		controller:vars[2],
		action:vars[3]
	}

	if (vars.length > 4) {
		completeInfo.params = mainURL.split(vars[3])[1];
	}

	return completeInfo;
}

function refreshPostContent(postid) {
	const post = findPostFromFeed(postid);

	$.get(
		'/tiktilaok/posts/getpostcontents.json',
		{
			'postid' : postid
		},
		function(data) {
			$(post).find("p.content").html(data.content);
			if (data.picture) {
				var imgTag = $(post).find("img.post-attached-picture");
				if(imgTag.length) {
					$(imgTag).attr('src',(`/tiktilaok/img/attached/${data.picture}`));
				} else {
					imgTag = document.createElement("img");
					imgTag.classList.add("post-attached-picture");
					imgTag.src = `/tiktilaok/img/attached/${data.picture}`;
					$(post).find("p.content").parent().append(imgTag);
				}

			}
		}
	);
}


